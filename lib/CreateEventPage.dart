import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project/PrefrencePage.dart';

import 'SecondAccountPage.dart';


class CreateEvent extends StatefulWidget {
  @override
  _CreateEvent createState() => _CreateEvent();
}

class _CreateEvent extends State<CreateEvent> {
  double _value = 20;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    child: Text("Create your",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                  ),
                  Container(
                    width: double.infinity,
                    child: Text("event",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    width: double.infinity,
                    child: Text("Fill your event details",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.home_work_outlined),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      labelText: 'Event Name',
                      hintText: 'Enter event name'
                  )
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Container(
                width: double.infinity,
                child: Text("Event Category",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
              ),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Wrap(
                children: <Widget>[
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () { },
                        child: Text('Movies',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Cooking',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Design',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Gambling',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Music Enthusiast',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Video Games',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Book Nerd',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Art',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Booking',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Athlete',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Shopping',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Technology',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Swimming',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 35,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Videography',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Container(
                width: double.infinity,
                child: Text("Select Date and Time",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: TextField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.location_on_outlined),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      labelText: 'Email',
                      hintText: 'Enter valid email id'
                  )
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10,0,10,0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.location_on_outlined),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      labelText: 'Location',
                      hintText: 'Enter location'
                  )
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextFormField(
                  minLines: 6,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.view_quilt),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      labelText: 'Location',
                      hintText: 'Enter location'
                  )
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Container(
                width: double.infinity,
                child: Text("Points",style: TextStyle(fontFamily: 'Rubik',fontSize: 18)),
              ),
            ),
            Container(
              child: Slider(
                min: 0.0,
                max: 500.0,
                value: _value,
                divisions: 5,
                onChanged: (value) {
                  setState(() {
                    _value = value;
                  });
                },
                label: "$_value",
              )
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
              child: Column(
                children: [
                  Container(
                      height: 50,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                      child: FlatButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => PrefrencePage()),);
                        },
                        child: Text('Next',
                          style: TextStyle(fontFamily: 'Rubik',color: Colors.white, fontSize: 18),
                        ),
                      )
                  )
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}