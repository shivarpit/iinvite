import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:project/MyEventPage.dart';

import 'CreateEventPage.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.filter_alt_outlined,
                  size: 26.0,
                  color: Colors.black,
                ),
              )
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.notifications_none,
                  size: 26.0,
                  color: Colors.black,
                ),
              )
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
             padding: EdgeInsets.all(10),
             margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
             width: double.infinity,
             child: Container(
               decoration: BoxDecoration(
                   border: Border.all(color: Colors.grey,width: 1),
                   borderRadius: BorderRadius.circular(10.0),
                   boxShadow: [
                     BoxShadow(
                       offset: Offset(0, 2),
                       blurRadius: 3,
                       color: Color.fromRGBO(0, 0, 0, 0.6),
                     )
                   ],
                 color: Colors.white
               ),
               child: Column(
                 children: [
                   Container(
                     child: Row(
                       children: [
                         Container(
                           padding: EdgeInsets.all(10.0),
                           width: 80,
                           height: 80,
                           child: Column(
                             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [
                               Image.asset("images/profile-1.png")
                             ],
                           ),
                         ),
                         Expanded(
                           child: Column(
                             children: <Widget>[
                               Container(
                                 width: double.infinity,
                                 padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                 child: Row(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                   children: <Widget>[
                                     Text(
                                       'Sunjana Mehta',
                                       style: TextStyle(
                                           fontSize: 20,
                                           fontFamily: "Rubik",
                                           fontWeight: FontWeight.w600
                                       ),
                                     ),
                                     Icon(Icons.access_time_filled_rounded,size: 20,color: Colors.blue,),
                                   ],
                                 ),
                               ),
                               Container(
                                   width: double.infinity,
                                   child: Text("30 Years",style: TextStyle(fontSize: 14,fontFamily: "Rubik"),)
                               ),
                               Container(
                                   width: double.infinity,
                                   padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                   child: Row(
                                     mainAxisAlignment: MainAxisAlignment.start,
                                     children: <Widget>[
                                       Icon(Icons.record_voice_over_outlined,size: 20,color: Colors.blue,),
                                       Text(
                                         '  English   Hind   Kannada',
                                         style: TextStyle(
                                             fontSize: 14,
                                             fontFamily: "Rubik"
                                         ),
                                       ),
                                     ],
                                   ),
                               ),
                               Container(
                                   width: double.infinity,
                                   padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                                   child: Text("Extrovert fun loving and enjoy new company...",style: TextStyle(fontSize: 14,fontFamily: "Rubik"))
                               ),
                             ],
                           ),
                         ),
                       ],
                     ),
                   ),
                   Container(
                     width: double.infinity,
                     decoration: BoxDecoration(
                       color: Color(0xFFedf9fe),
                       border: Border(bottom: BorderSide(width: 1, color: Colors.blue))
                     ),
                     padding: EdgeInsets.all(10),
                     child: Column(
                       children: [
                         Container(
                           width:double.infinity,
                           child: Text("Coffee at CCD",style: TextStyle(fontSize: 22,fontFamily: "Rubik",fontWeight: FontWeight.w600,color: Colors.blue),),
                         ),
                         Container(
                           width:double.infinity,
                           child: Text("Enjoying Coffee at Rajaji Nagar CCD on",style: TextStyle(fontSize: 14,fontFamily: "Rubik")),
                         ),
                         Container(
                           width:double.infinity,
                           child: Text("Sunday evening No Flings just formal talk",style: TextStyle(fontSize: 14,fontFamily: "Rubik")),
                         ),
                         Container(
                           padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                           child: Row(
                               children:<Widget>[
                                 Expanded(
                                   flex:1,
                                     child: Container(
                                       width: MediaQuery.of(context).size.width * 0.50,
                                       child: Row(
                                         mainAxisAlignment: MainAxisAlignment.start,
                                         children: <Widget>[
                                           Icon(Icons.location_on_outlined,size: 14,color: Colors.blue,),
                                           Text(
                                             ' Rajaji Nagar, BL IN',
                                             style: TextStyle(
                                               fontSize: 14,
                                               color: Colors.blue
                                             ),
                                           ),
                                         ],
                                       ),
                                     )
                                 ),
                                 Expanded(
                                     flex:1,
                                     child: Container(
                                       width: MediaQuery.of(context).size.width * 0.50,
                                       child: Row(
                                         children: <Widget>[
                                           Icon(Icons.access_time,size: 14,color: Colors.blue,),
                                           Text(
                                             ' Sun, Nov 7 at 6PM',
                                             style: TextStyle(
                                               fontSize: 14,
                                               color: Colors.blue
                                             ),
                                           ),
                                         ],
                                       ),
                                     )
                                 ),
                               ]
                           ),
                         ),
                         Container(
                           child: Row(
                               children:<Widget>[
                                 Expanded(
                                     flex:1,
                                     child: Container(
                                       width: MediaQuery.of(context).size.width * 0.50,
                                       child: Row(
                                         mainAxisAlignment: MainAxisAlignment.start,
                                         children: <Widget>[
                                           Icon(Icons.hourglass_empty_sharp,size: 14,),
                                           Text(
                                             ' 20:30',
                                             style: TextStyle(
                                               fontSize: 14,
                                             ),
                                           ),
                                         ],
                                       ),
                                     )
                                 ),
                                 Expanded(
                                     flex:1,
                                     child: Container(
                                       width: MediaQuery.of(context).size.width * 0.50,
                                       child: Row(
                                         children: <Widget>[
                                           Icon(Icons.credit_card_sharp,size: 14,),
                                           Text(
                                             ' 50 Points',
                                             style: TextStyle(
                                               fontSize: 14,
                                             ),
                                           ),
                                         ],
                                       ),
                                     )
                                 ),
                               ]
                           ),
                         )
                       ],
                     ),
                   ),
                   Container(
                     padding: EdgeInsets.all(10),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceAround,
                       children:<Widget>[
                         Container(
                           child: Icon(Icons.delete_outline)
                         ),
                         Container(
                           child: Icon(Icons.person_add_alt_outlined)
                         ),
                         Container(
                           child: Icon(Icons.bookmark_outline_sharp)
                         )
                       ]
                     ),
                   )
                 ],
               )
             ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
              width: double.infinity,
              child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey,width: 1),
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 2),
                          blurRadius: 3,
                          color: Color.fromRGBO(0, 0, 0, 0.6),
                        )
                      ],
                      color: Colors.white
                  ),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Container(
                              padding: EdgeInsets.all(10.0),
                              width: 80,
                              height: 80,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image.asset("images/profile-1.png")
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: double.infinity,
                                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          'Sunjana Mehta',
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontFamily: "Rubik",
                                              fontWeight: FontWeight.w600
                                          ),
                                        ),
                                        Icon(Icons.access_time_filled_rounded,size: 20,color: Colors.blue,),
                                      ],
                                    ),
                                  ),
                                  Container(
                                      width: double.infinity,
                                      child: Text("30 Years",style: TextStyle(fontSize: 14,fontFamily: "Rubik"),)
                                  ),
                                  Container(
                                    width: double.infinity,
                                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Icon(Icons.record_voice_over_outlined,size: 20,color: Colors.blue,),
                                        Text(
                                          '  English   Hind   Kannada',
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: "Rubik"
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                      width: double.infinity,
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                                      child: Text("Extrovert fun loving and enjoy new company...",style: TextStyle(fontSize: 14,fontFamily: "Rubik"))
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Color(0xFFedf9fe),
                            border: Border(bottom: BorderSide(width: 1, color: Colors.blue))
                        ),
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Container(
                              width:double.infinity,
                              child: Text("Coffee at CCD",style: TextStyle(fontSize: 22,fontFamily: "Rubik",fontWeight: FontWeight.w600,color: Colors.blue),),
                            ),
                            Container(
                              width:double.infinity,
                              child: Text("Enjoying Coffee at Rajaji Nagar CCD on",style: TextStyle(fontSize: 14,fontFamily: "Rubik")),
                            ),
                            Container(
                              width:double.infinity,
                              child: Text("Sunday evening No Flings just formal talk",style: TextStyle(fontSize: 14,fontFamily: "Rubik")),
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              child: Row(
                                  children:<Widget>[
                                    Expanded(
                                        flex:1,
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.50,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              Icon(Icons.location_on_outlined,size: 14,color: Colors.blue,),
                                              Text(
                                                ' Rajaji Nagar, BL IN',
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.blue
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    ),
                                    Expanded(
                                        flex:1,
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.50,
                                          child: Row(
                                            children: <Widget>[
                                              Icon(Icons.access_time,size: 14,color: Colors.blue,),
                                              Text(
                                                ' Sun, Nov 7 at 6PM',
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.blue
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    ),
                                  ]
                              ),
                            ),
                            Container(
                              child: Row(
                                  children:<Widget>[
                                    Expanded(
                                        flex:1,
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.50,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              Icon(Icons.hourglass_empty_sharp,size: 14,),
                                              Text(
                                                ' 20:30',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    ),
                                    Expanded(
                                        flex:1,
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.50,
                                          child: Row(
                                            children: <Widget>[
                                              Icon(Icons.credit_card_sharp,size: 14,),
                                              Text(
                                                ' 50 Points',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    ),
                                  ]
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children:<Widget>[
                              Container(
                                  child: Icon(Icons.delete_outline)
                              ),
                              Container(
                                  child: Icon(Icons.person_add_alt_outlined)
                              ),
                              Container(
                                  child: Icon(Icons.bookmark_outline_sharp)
                              )
                            ]
                        ),
                      )
                    ],
                  )
              ),
            ),
          ]
        )
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 60,
          padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.home_outlined,size: 30,color: Colors.blue),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),);
                },
              ),
              IconButton(
                icon: Icon(Icons.calendar_today_rounded,size: 30),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyEventPage()),);
                },
              ),
              IconButton(
                padding: EdgeInsets.all(0),
                alignment: Alignment.topCenter,
                icon: Icon(Icons.add_circle_rounded,size:56),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CreateEvent()),);
                },
              ),
              IconButton(
                icon: Icon(Icons.near_me_sharp,size: 30),
                onPressed: (){},
              ),
              IconButton(
                icon: Icon(Icons.people,size: 30),
                onPressed: (){},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
