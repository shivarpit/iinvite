import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'LoginWebPage.dart';
import 'SecondAccountPage.dart';


class CreateAccount extends StatefulWidget {
  @override
  _CreateAccount createState() => _CreateAccount();
}

class _CreateAccount extends State<CreateAccount> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Text("Create your",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                ),
                Container(
                  width: double.infinity,
                  child: Text("account!",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  width: double.infinity,
                  child: Text("Sign up and get started!",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.people),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    labelText: 'Name',
                    hintText: 'Enter User Name'
                )
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    labelText: 'Email',
                    hintText: 'Enter valid email id'
                )
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10,0,10,0),
            child: TextField(
                obscureText: true,
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    labelText: 'Password',
                    hintText: 'Enter Password'
                )
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: Column(
              children: [
                Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => SecondAccountPage()),);
                      },
                      child: Text('Sign Up',
                        style: TextStyle(fontFamily: 'Rubik',color: Colors.white, fontSize: 18),
                      ),
                    )
                )
              ],
            ),
          ),
          Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 30),
              child: Center(
                child: Container(
                  child: Text("Or sign in with social account",style: TextStyle(fontFamily: 'Rubik',fontSize: 14,color: Colors.black)),
                ),
              )
          ),
          Container(
            child: Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                          height: 70,
                          onPressed: () => {},
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                "images/facebook.png",
                                height: 25.0,
                              ),
                              SizedBox(height: 5),
                              Container(
                                  padding: EdgeInsets.only(left: 10.0,right: 10.0),
                                  child: new Text("Facebook",style: TextStyle(fontFamily: 'Rubik',color: Colors.grey,fontWeight: FontWeight.bold),)
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(10)
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0,10,0,10),
                        child: FlatButton(
                          height: 70,
                          minWidth: 100,
                          onPressed: () => {},
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                "images/google.png",
                                height: 25.0,
                              ),
                              SizedBox(height: 5),
                              Container(
                                  padding: EdgeInsets.only(left: 10.0,right: 10.0),
                                  child: new Text("Google",style: TextStyle(fontFamily: 'Rubik',color: Colors.grey,fontWeight: FontWeight.bold),)
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(10)
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                          height: 70,
                          onPressed: () => {},
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                'images/whatsapp.png',
                                height: 25.0,
                              ),
                              SizedBox(height: 5),
                              Container(
                                  padding: EdgeInsets.only(left: 10.0,right: 10.0),
                                  child: new Text("Whatsapp",style: TextStyle(fontFamily: 'Rubik',color: Colors.grey,fontWeight: FontWeight.bold),)
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(10)
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text("Already have account?",style: TextStyle(fontFamily: 'Rubik'),),
                      ),
                      Container(
                          child: FlatButton(
                            color: Colors.transparent,
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => LoginWebPage()),);
                            },
                            child: Text("Log in",style: TextStyle(fontFamily: 'Rubik',color: Colors.black,fontWeight: FontWeight.bold)),
                          )
                      )
                    ]
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}