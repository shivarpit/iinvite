import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project/EditMyProfile.dart';

import 'CreateEventPage.dart';
import 'HomePage.dart';
import 'MyEventListPage.dart';
import 'SettingPage.dart';


class MyPorfilePage extends StatefulWidget {
  const MyPorfilePage({Key? key}) : super(key: key);

  @override
  _MyPorfilePageState createState() => _MyPorfilePageState();
}

class _MyPorfilePageState extends State<MyPorfilePage> {
  TextStyle activeTabLabelStyle = new TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold);
  TextStyle tabLabelStyle = new TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Profile",style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold,color: Colors.black),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SettingPage()),);
              },
              child: Icon(
              Icons.settings,
              size: 26.0,
              color: Colors.black,
              ),
            )
          )
        ],
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(10.0),
                    width: MediaQuery.of(context).size.width * 0.46,
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image(
                          image: AssetImage('images/profile-1.png'),
                          alignment: Alignment.center,
                          height: 130,
                          width: 130,
                          fit: BoxFit.fill,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: Wrap(
                            children: <Widget>[
                              Text(
                                'Sunjana Mehta',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontFamily: "Rubik",
                                    fontWeight: FontWeight.w600
                                ),
                              ),
                              Icon(Icons.access_time_filled_rounded,size: 20,color: Colors.blue,),
                            ],
                          ),
                        ),
                        Container(
                            width: double.infinity,
                            child: Text("Software Developer",style: TextStyle(fontSize: 18,fontFamily: "Rubik"),)
                        ),
                        Container(
                            width: double.infinity,
                            child: Text("30 Years",style: TextStyle(fontSize: 14,fontFamily: "Rubik"),)
                        ),
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Icon(Icons.record_voice_over_outlined,size: 20,color: Colors.blue,),
                              Text(
                                '  English   Hind   Kannada',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: "Rubik"
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(Icons.location_on_outlined,size: 20,color: Colors.blue,),
                              Text(
                                '  Bangalone, IN',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: "Rubik"
                                ),
                              ),
                            ],
                          )
                        ),
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Wrap(
                            children: [
                              Container(
                                height: 35,
                                width: 35,
                                child: Image.asset(
                                  "images/messenger.png",height: 20,
                                )
                              ),
                              Container(
                                height: 35,
                                width: 35,
                                  child: Image.asset(
                                    "images/instagram.png",height: 20,
                                  )
                              ),
                              Container(
                                height: 35,
                                width: 35,
                                child: Image.asset(
                                    "images/linkedin.png",height: 20,
                                )
                              ),
                              Container(
                                height: 35,
                                width: 35,
                                  child: Image.asset(
                                    "images/email.png",height: 20,
                                  )
                              ),
                              Container(
                                height: 35,
                                width: 35,
                                  child: Image.asset(
                                    "images/phone-call.png",height: 20,
                                  )
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Text("Lorem ipsum dolor sit amet. consectetur adipiscringelit. sed do eiusmod tempor incididunt ut labore etdolore magna aliqua.",),
            ),
            Container(
                padding: EdgeInsets.all(10),
                width: double.infinity,
                child: Container(
                  height: 40,
                  child: RaisedButton(
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => EditMyPorfile()),);
                    },
                    child: Text(' Edit Profile ',style: TextStyle(fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                    color: Colors.white,
                    textColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.grey, width: 1),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  ),
                )
            ),
            Container(
              padding: EdgeInsets.all(10),
              width: double.infinity,
              child: Text(" Interests ",style: TextStyle(fontSize: 22,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Wrap(
                children: <Widget>[
                  Container(
                      height: 45,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () { },
                        child: Text('Movies',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 45,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Cooking',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                  Container(
                      height: 45,
                      margin: EdgeInsets.only(bottom: 10,right: 10),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Design',
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        ),
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      )
                  ),
                ]
              )
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              width: double.infinity,
              child: Text(" Gallery ",style: TextStyle(fontSize: 22,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Image.asset(
                        "images/features-1.png",width: double.infinity,
                      ),
                    )
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Image.asset(
                        "images/features-1.png",width: double.infinity,
                      ),
                    )
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Image.asset(
                        "images/features-1.png",width: double.infinity,
                      ),
                    )
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Row(
                children: [
                  Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                        child: Image.asset(
                          "images/features-1.png",width: double.infinity,
                        ),
                      )
                  ),
                  Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                        child: Image.asset(
                          "images/features-1.png",width: double.infinity,
                        ),
                      )
                  ),
                  Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                        child: Image.asset(
                          "images/features-1.png",width: double.infinity,
                        ),
                      )
                  ),
                ],
              ),
            ),
          ],
        )
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 60,
          padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.home_outlined,size: 30),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),);
                },
              ),
              IconButton(
                icon: Icon(Icons.calendar_today_rounded,size: 30),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyEventListPage()),);
                },
              ),
              IconButton(
                padding: EdgeInsets.all(0),
                alignment: Alignment.topCenter,
                icon: Icon(Icons.add_circle_rounded,size:56),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CreateEvent()),);
                },
              ),
              IconButton(
                icon: Icon(Icons.near_me_sharp,size: 30),
                onPressed: (){},
              ),
              IconButton(
                icon: Icon(Icons.people,size: 30,color: Colors.blue),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyPorfilePage()),);
                },
              ),
            ],
          ),
        ),
      )
    );
  }
}
