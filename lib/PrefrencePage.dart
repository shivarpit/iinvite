import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'SecondAccountPage.dart';


class PrefrencePage extends StatefulWidget {
  @override
  _PrefrencePage createState() => _PrefrencePage();
}

class _PrefrencePage extends State<PrefrencePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Text("Your",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                ),
                Container(
                  width: double.infinity,
                  child: Text("Prefrences",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  width: double.infinity,
                  child: Text("Choose your preferences",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 30, 10, 5),
            width: double.infinity,
            child: Text("Smoking Habit",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
          ),
          Row(
            children: [
              Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.46,
                  margin: EdgeInsets.only(left: 10,right: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 1,color: Colors.grey)
                  ),
                  child: FlatButton(
                    onPressed: () {},
                    child: Text('Yes',
                      style: TextStyle(fontFamily: 'Rubik',color: Colors.grey, fontSize: 18),
                    ),
                  )
              ),
              Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.46,
                  decoration: BoxDecoration(
                      color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                  child: FlatButton(
                    onPressed: () {},
                    child: Text('No',
                      style: TextStyle(fontFamily: 'Rubik',color: Colors.white, fontSize: 18),
                    ),
                  )
              )
            ],
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 30, 10, 5),
            width: double.infinity,
            child: Text("Gender",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
          ),
          Wrap(
            children: [
              Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.46,
                  margin: EdgeInsets.only(right: 10,bottom: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1,color: Colors.grey)
                  ),
                  child: FlatButton(
                    onPressed: () {},
                    child: Text('Male',
                      style: TextStyle(fontFamily: 'Rubik',color: Colors.grey, fontSize: 18),
                    ),
                  )
              ),
              Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.46,
                  margin: EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                      color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                  child: FlatButton(
                    onPressed: () {},
                    child: Text('Female',
                      style: TextStyle(fontFamily: 'Rubik',color: Colors.white, fontSize: 18),
                    ),
                  )
              ),
              Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.46,
                  margin: EdgeInsets.only(right: 10,bottom: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1,color: Colors.grey)
                  ),
                  child: FlatButton(
                    onPressed: () {},
                    child: Text('Neuter',
                      style: TextStyle(fontFamily: 'Rubik',color: Colors.grey, fontSize: 18),
                    ),
                  )
              ),
              Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.46,
                  margin: EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 1,color: Colors.grey)
                  ),
                  child: FlatButton(
                    onPressed: () {},
                    child: Text('Common',
                      style: TextStyle(fontFamily: 'Rubik',color: Colors.grey, fontSize: 18),
                    ),
                  )
              )
            ],
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: Column(
              children: [
                Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Create Event',
                        style: TextStyle(fontFamily: 'Rubik',color: Colors.white, fontSize: 18),
                      ),
                    )
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}