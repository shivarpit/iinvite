import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:dotted_border/dotted_border.dart';

import 'InterestUserPage.dart';


void main() {
  runApp(PhotosUserPage());
}
class PhotosUserPage extends StatefulWidget {
  @override
  _PhotosUserPage createState() => _PhotosUserPage();
}

class _PhotosUserPage extends State<PhotosUserPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Text("Upload your",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  width: double.infinity,
                  child: Text("photos",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  width: double.infinity,
                  child: Text("add your atleast 2 best photos",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                )
              ],
            ),
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(10),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: <Widget>[
                Container(
                  height:120,
                  width: MediaQuery.of(context).size.width * 0.46,
                  margin: EdgeInsets.only(bottom:10,right: 10),
                  decoration: BoxDecoration(
                    color: Color(0xFFedf9ff)
                  ),
                  child: DottedBorder(
                    child: Icon(Icons.add,color:Colors.blue),
                    borderType: BorderType.RRect,
                    radius: Radius.circular(20),
                    dashPattern: [10,5,10,5,10,5],
                    color: Colors.blue,
                  )
                ),
                Container(
                    height:120,
                    width: MediaQuery.of(context).size.width * 0.46,
                    margin: EdgeInsets.only(bottom:10),
                    decoration: BoxDecoration(
                        color: Color(0xFFedf9ff)
                    ),
                    child: DottedBorder(
                      child: Icon(Icons.add,color:Colors.blue),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(20),
                      dashPattern: [10,5,10,5,10,5],
                      color: Colors.blue,
                    )
                ),
                Container(
                    height:120,
                    width: MediaQuery.of(context).size.width * 0.46,
                    margin: EdgeInsets.only(bottom:10,right: 10),
                    decoration: BoxDecoration(
                        color: Color(0xFFedf9ff)
                    ),
                    child: DottedBorder(
                      child: Icon(Icons.add,color:Colors.blue),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(20),
                      dashPattern: [10,5,10,5,10,5],
                      color: Colors.blue,
                    )
                ),
                Container(
                    height:120,
                    width: MediaQuery.of(context).size.width * 0.46,
                    margin: EdgeInsets.only(bottom:10),
                    decoration: BoxDecoration(
                        color: Color(0xFFedf9ff)
                    ),
                    child: DottedBorder(
                      child: Icon(Icons.add,color:Colors.blue),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(20),
                      dashPattern: [10,5,10,5,10,5],
                      color: Colors.blue,
                    )
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: Column(
              children: [
                Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => InterestUserPage()),);
                      },
                      child: Text('Continue',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    )
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
