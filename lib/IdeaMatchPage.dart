import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


import 'VerifyAccountPage.dart';


void main() {
  runApp(IdeaMatchPage());
}
class IdeaMatchPage extends StatefulWidget {
  @override
  _IdeaMatchPage createState() => _IdeaMatchPage();
}

class _IdeaMatchPage extends State<IdeaMatchPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
       child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Text("Ideal match",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  width: double.infinity,
                  child: Text("for you",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  width: double.infinity,
                  child: Text("What are you hoping to find on the iInvite",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                )
              ],
            ),
          ),
          Container(
            child: Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width * 0.50,
                        child: FlatButton(
                          height: 180,
                          onPressed: () => {},
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Color(0xffedf9fe),
                                ),
                                child: Icon(Icons.favorite_outline,color: Colors.blue,),
                              ),
                              SizedBox(height: 30),
                              Container(
                                  padding: EdgeInsets.only(left: 10.0,right: 10.0),
                                  child: new Text("New People",style: TextStyle(fontFamily: 'Rubik',color: Colors.grey,fontWeight: FontWeight.bold),)
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(10)
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width * 0.50,
                        child: FlatButton(
                          height: 180,
                          minWidth: 100,
                          onPressed: () => {},
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Color(0xffedf9fe),
                                ),
                                child: Icon(Icons.person_search_outlined,color: Colors.blue,),
                              ),
                              SizedBox(height: 30),
                              Container(
                                  padding: EdgeInsets.only(left: 10.0,right: 10.0),
                                  child: new Text("Friend",style: TextStyle(fontFamily: 'Rubik',color: Colors.grey,fontWeight: FontWeight.bold),)
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(10)
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width * 0.50,
                        child: FlatButton(
                          height: 180,
                          onPressed: () => {},
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Color(0xffedf9fe),
                                ),
                                child: Icon(Icons.free_breakfast_outlined,color: Colors.blue,),
                              ),
                              SizedBox(height: 30),
                              Container(
                                  padding: EdgeInsets.only(left: 10.0,right: 10.0),
                                  child: new Text("Gathering",style: TextStyle(fontFamily: 'Rubik',color: Colors.grey,fontWeight: FontWeight.bold),)
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(10)
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width * 0.50,
                        child: FlatButton(
                          height: 180,
                          minWidth: 100,
                          onPressed: () => {},
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Color(0xffedf9fe),
                                ),
                                child: Icon(Icons.business_center_outlined,color: Colors.blue,),
                              ),
                              SizedBox(height: 30),
                              Container(
                                  padding: EdgeInsets.only(left: 10.0,right: 10.0),
                                  child: new Text("Business",style: TextStyle(fontFamily: 'Rubik',color: Colors.grey,fontWeight: FontWeight.bold),)
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(10)
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: Column(
              children: [
                Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => VerifyAccountPage()),);
                      },
                      child: Text('Continue',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    )
                )
              ],
            ),
          ),
        ],
      ),
      )
    );
  }
}
