import 'package:flutter/material.dart';
import 'package:project/HomePage.dart';

import 'CouokedPage.dart';
import 'LoginWebPage.dart';


class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      /*appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),*/
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 50,
              width: 300,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: ExactAssetImage('assets/images/logo.png'),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:30),
              child: Text('Engage with Events', style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Rubik',
                )
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:10),
              child: Text('Sign in and create event and', style: TextStyle(
                fontSize: 18,
                color: Colors.grey,
                fontFamily: 'Rubik'
               ),
              ),
            ),
            Container(
              child: Text('invite people now!',style: TextStyle(
                fontSize: 18,
                color: Colors.grey,
                fontFamily: 'Rubik'
              ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 40, 10, 0),
              child: Column(
                children: [
                  Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white, borderRadius: BorderRadius.circular(10)),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginWebPage()),);
                      },
                      child: Text('Do you already have an account?',
                          style: TextStyle(color: Colors.black, fontSize: 18,fontFamily: 'Rubik'),
                      ),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.grey, width: 1),
                        borderRadius: BorderRadius.circular(10)
                      ),
                    )
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
              child: Column(
                children: [
                  Container(
                      height: 50,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                      child: FlatButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => CoupledPage()),);
                        },
                        child: Text('Get Started',
                          style: TextStyle(color: Colors.white, fontSize: 18,fontFamily: 'Rubik'),
                        ),
                      )
                  )
                ],
              ),
            )
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
