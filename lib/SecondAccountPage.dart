import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import 'PhotosUserPage.dart';


void main() {
  runApp(SecondAccountPage());
}
class SecondAccountPage extends StatefulWidget {
  @override
  _SecondAccountPage createState() => _SecondAccountPage();
}
class Animal {
  final String name;

  Animal({
    required this.name,
  });
}
class _SecondAccountPage extends State<SecondAccountPage> {
  final items=['Male','Femail','Other'];
  String? value;
  static List<Animal> _animals = [
    Animal(name: "English"),
    Animal(name: "Hindi"),
    Animal(name: "Punjabi"),
    Animal(name: "Telgu"),
    Animal(name: "Assamese"),
    Animal(name: "Gujarati"),
    Animal(name: "Urdu"),
    Animal(name: "Kannada"),
    Animal(name: "Malayalam"),
    Animal(name: "Marathi"),
    Animal(name: "Oriya"),
    Animal(name: "Tamil"),
    Animal(name: "Bengali"),
  ];
  final _items = _animals
      .map((animal) => MultiSelectItem<Animal>(animal, animal.name))
      .toList();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Text("Create your",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  width: double.infinity,
                  child: Text("account!",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  width: double.infinity,
                  child: Text("Sign up and get started!",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.calendar_today),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    labelText: 'Date of Birth',
                    hintText: 'Date of Birth'
                )
            ),
          ),
          Container(
            child: Container(
              padding: EdgeInsets.fromLTRB(10,0,10,0),
              margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
              decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.grey,width: 1)
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: Text("Gender"),
                  value: value,
                  iconSize:30,
                  icon: Icon(Icons.arrow_drop_down,color:Colors.grey),
                  isExpanded:true,
                  items: items.map(buildMenuItem).toList(),
                  onChanged: (value) => setState(() => this.value = value),
                ),
              )
            )
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.map),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    labelText: 'Country',
                    hintText: 'Country'
                )
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10,0,10,0),
            child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.map),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    labelText: 'City',
                    hintText: 'Enter City'
                )
            ),
          ),
          Container(
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Column(
                  children: [
                    MultiSelectDialogField(
                      items: _items,
                      title: Text("Animals"),
                      selectedColor: Colors.blue,
                      buttonIcon: Icon(
                        Icons.language,
                        color: Colors.grey,
                      ),
                      buttonText: Text(
                        "Language"
                      ),
                      onConfirm: (results) {
                        //_selectedAnimals = results;
                      },
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.grey,width: 1)
                      ),
                    ),
                  ],
                )
              )
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: Column(
              children: [
                Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PhotosUserPage()),);
                      },
                      child: Text('Continue',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    )
                )
              ],
            ),
          ),
        ],
      ),
      )
    );
  }
  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
    value: item,
    child: Text(
      item,
      style: TextStyle(fontSize: 20),
    ),
  );
}
