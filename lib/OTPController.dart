import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:project/HomePage.dart';

class OTPControllerScreen extends StatefulWidget {
  final String phone;
  final String codeDigits;
  OTPControllerScreen({required this.phone, required this.codeDigits});

  @override
  _OTPControllerScreenState createState() => _OTPControllerScreenState();
}

class _OTPControllerScreenState extends State<OTPControllerScreen> {
  final GlobalKey<ScaffoldState> _scaffolkey = GlobalKey<ScaffoldState>();
  final TextEditingController _pinOTPCodeController = TextEditingController();
  final FocusNode _pinOTPCodeFoucus = FocusNode();
  String? varificationcode;

  final BoxDecoration pinOTPCodeDecoration = BoxDecoration(
    color: Colors.grey,
    borderRadius: BorderRadius.circular(10),
    border: Border.all(
      color: Colors.white10
    )
  );

  @override
  void initState(){
    super.initState();
    verifyPhoneNumber();
  }
  verifyPhoneNumber() async{
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: "${widget.codeDigits + widget.phone}",
        verificationCompleted: (PhoneAuthCredential credential) async{
          await FirebaseAuth.instance.signInWithCredential(credential).then((value){
            if(value.user != null){
              Navigator.of(context).push(MaterialPageRoute(builder: (c)=> HomeScreen()));
            }
          });
        },
        verificationFailed: (FirebaseAuthException e){
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(e.message.toString()),
                duration: Duration(seconds: 3),
              ),
          );
        },
        codeSent: (String vID, int? resendToken){
          setState(() {
            varificationcode = vID;
          });
        },
        codeAutoRetrievalTimeout: (String vID){
          setState(() {
            varificationcode = vID;
          });
        },
        timeout: Duration(seconds: 40),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(padding: const EdgeInsets.all(0.0),child: Image.asset("images/logo.png"),
          ),
          Container(
            margin: EdgeInsets.only(top:20),
            child: Center(
              child: GestureDetector(
                onTap: (){
                  verifyPhoneNumber();
                },
                child: Text('Verifying: ${widget.codeDigits}-${widget.phone}',
                  style: TextStyle(fontSize: 16,fontWeight: FontWeight.w300,fontFamily: 'Rubik'),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(40.0),
            child: PinPut(
              fieldsCount: 6,
              textStyle: TextStyle(fontSize: 25.0,color: Colors.black),
              eachFieldHeight: 40.0,
              eachFieldWidth: 40.0,
              focusNode: _pinOTPCodeFoucus,
              controller: _pinOTPCodeController,
              submittedFieldDecoration: pinOTPCodeDecoration,
              selectedFieldDecoration: pinOTPCodeDecoration,
              followingFieldDecoration: pinOTPCodeDecoration,
              onSubmit: (pin) async{
                try{
                  await FirebaseAuth.instance
                      .signInWithCredential(PhoneAuthProvider
                      .credential(verificationId: varificationcode!, smsCode: pin))
                      .then((value){
                        if(value.user != null){
                          Navigator.of(context).push(MaterialPageRoute(builder: (c)=> HomeScreen()));
                        }
                  });
                }
                catch(e){
                  Navigator.of(context).push(MaterialPageRoute(builder: (c)=> HomeScreen()));
                  /*FocusScope.of(context).unfocus();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Invalid OTP ${pin}"),
                      duration: Duration(seconds: 3),

                    ),

                  );*/
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
