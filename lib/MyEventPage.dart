import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:project/BookmarkEventPage.dart';
import 'package:project/FavoritEventPage.dart';

import 'CreateEventPage.dart';
import 'HomePage.dart';

class MyEventPage extends StatefulWidget {
  const MyEventPage({Key? key}) : super(key: key);

  @override
  _MyEventPageState createState() => _MyEventPageState();
}

class _MyEventPageState extends State<MyEventPage> {
  TextStyle activeTabLabelStyle = new TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold);
  TextStyle tabLabelStyle = new TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length:3,
      child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF19b5f8),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.filter_alt_outlined,
                  size: 26.0,
                  color: Colors.white,
                ),
              )
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.notifications_none,
                  size: 26.0,
                  color: Colors.white,
                ),
              )
          )
        ],
        bottom: TabBar(
          labelPadding: EdgeInsets.all(6),
          labelStyle: activeTabLabelStyle,
          unselectedLabelStyle: tabLabelStyle,
          tabs: <Widget>[
            Text("MY Events"),
            Text("Favourite"),
            Text("Bookmark")
          ],
        ),
      ),


      body: SingleChildScrollView(
          padding: EdgeInsets.all(10.0),
          child: Column(
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Card(
                        color: Color(0xFF19b5f8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(width: 1,color: Color(0xFF19b5f8))
                        ),
                        child: TextButton(
                          onPressed: (){},
                          child: Container(
                            padding: EdgeInsets.all(10),
                            width: MediaQuery.of(context).size.width * 0.40,
                            child: Column(
                              children: [
                                Container(
                                  width: double.infinity,
                                  child: Text("Coffee at CCD",style: TextStyle(color: Colors.white,fontFamily: "Rubik",fontSize: 14)),
                                ),
                                Container(
                                  width: double.infinity,
                                  child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Text("23 Apr 21",style: TextStyle(color: Colors.white,fontFamily: "Rubik",fontSize: 14)),
                                        new Text("00:45",style: TextStyle(color: Colors.white,fontFamily: "Rubik",fontSize: 14))
                                      ]
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ),
                      Card(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                            side: BorderSide(width: 1,color: Colors.grey)
                          ),
                          child: TextButton(
                            onPressed: (){},
                            child: Container(
                              padding: EdgeInsets.all(10),
                              width: MediaQuery.of(context).size.width * 0.40,
                              child: Column(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    child: Text("Coffee at CCD",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14)),
                                  ),
                                  Container(
                                    width: double.infinity,
                                    child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        new Text("23 Apr 21",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14)),
                                        new Text("00:45",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14))
                                      ]
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0,10,0,10),
                  width: double.infinity,
                  child: Text("5 Requests Recieved",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
                  width: double.infinity,
                  child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey,width: 1),
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 2),
                              blurRadius: 3,
                              color: Color.fromRGBO(0, 0, 0, 0.6),
                            )
                          ],
                          color: Colors.white
                      ),
                      child: Column(
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(10.0),
                                  width: 80,
                                  height: 80,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Image.asset("images/profile-1.png")
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Sunjana Mehta',
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontFamily: "Rubik",
                                                  fontWeight: FontWeight.w600
                                              ),
                                            ),
                                            Icon(Icons.access_time_filled_rounded,size: 20,color: Colors.blue,),
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: double.infinity,
                                          child: Text("30 Years",style: TextStyle(fontSize: 14,fontFamily: "Rubik"),)
                                      ),
                                      Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Icon(Icons.record_voice_over_outlined,size: 20,color: Colors.blue,),
                                            Text(
                                              '  English   Hind   Kannada',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Rubik"
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: double.infinity,
                                          padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                                          child: Text("Extrovert fun loving and enjoy new company...",style: TextStyle(fontSize: 14,fontFamily: "Rubik"))
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              border: Border(top: BorderSide(width: 1, color: Colors.grey)),
                              ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children:<Widget>[
                                Container(
                                  child:Row(
                                    children: [
                                      Container(
                                        child: Icon(Icons.delete_outline,color: Colors.black,)
                                      ),
                                      Container(
                                        child: Text("Reject",style: TextStyle(fontSize: 12)),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child:Row(
                                    children: [
                                      Container(
                                          child: Icon(Icons.person_add_alt_outlined,color: Colors.blue,)
                                      ),
                                      Container(
                                        child: Text("Approve",style: TextStyle(fontSize: 12,color: Colors.blue)),
                                      )
                                    ],
                                  ),
                                )
                              ]
                            ),
                          )
                        ],
                      )
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
                  width: double.infinity,
                  child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey,width: 1),
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 2),
                              blurRadius: 3,
                              color: Color.fromRGBO(0, 0, 0, 0.6),
                            )
                          ],
                          color: Colors.white
                      ),
                      child: Column(
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(10.0),
                                  width: 80,
                                  height: 80,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Image.asset("images/profile-1.png")
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Sunjana Mehta',
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontFamily: "Rubik",
                                                  fontWeight: FontWeight.w600
                                              ),
                                            ),
                                            Icon(Icons.access_time_filled_rounded,size: 20,color: Colors.blue,),
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: double.infinity,
                                          child: Text("30 Years",style: TextStyle(fontSize: 14,fontFamily: "Rubik"),)
                                      ),
                                      Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Icon(Icons.record_voice_over_outlined,size: 20,color: Colors.blue,),
                                            Text(
                                              '  English   Hind   Kannada',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Rubik"
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: double.infinity,
                                          padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                                          child: Text("Extrovert fun loving and enjoy new company...",style: TextStyle(fontSize: 14,fontFamily: "Rubik"))
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              border: Border(top: BorderSide(width: 1, color: Colors.grey)),
                            ),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children:<Widget>[
                                  Container(
                                    child:Row(
                                      children: [
                                        Container(
                                            child: Icon(Icons.delete_outline,color: Colors.black,)
                                        ),
                                        Container(
                                          child: Text("Reject",style: TextStyle(fontSize: 12)),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child:Row(
                                      children: [
                                        Container(
                                            child: Icon(Icons.person_add_alt_outlined,color: Colors.blue,)
                                        ),
                                        Container(
                                          child: Text("Approve",style: TextStyle(fontSize: 12,color: Colors.blue)),
                                        )
                                      ],
                                    ),
                                  )
                                ]
                            ),
                          )
                        ],
                      )
                  ),
                ),
                Card(
                  color: Color(0xFFedf9fe),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: TextButton(
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => FavoritEventPage()),);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 20, 10, 5),
                            child: Text("Favorite Events",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 20,fontWeight: FontWeight.bold)),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Text("Events which you were interested",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14)),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(Icons.calendar_today_rounded,size: 14,color: Colors.blue,),
                                Text(
                                  ' 2 Events',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.blue,
                                      fontFamily: "Rubik"
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10,0,10,10),
                            child: Row(
                                children:<Widget>[
                                  Expanded(
                                      flex:1,
                                      child: Container(
                                        width: MediaQuery.of(context).size.width * 0.50,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Icon(Icons.person_add_alt_outlined,size: 14,color: Colors.blue,),
                                            Text(
                                              ' 15 Requests Received',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: Colors.blue
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                  ),
                                  Expanded(
                                      flex:1,
                                      child: Container(
                                        width: MediaQuery.of(context).size.width * 0.50,
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              width: MediaQuery.of(context).size.width * 0.20,
                                              child: Stack(
                                                //alignment:new Alignment(x, y)
                                                children: <Widget>[
                                                  new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                                  new Positioned(
                                                    left: 20.0,
                                                    child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                                  ),
                                                  new Positioned(
                                                    left:40.0,
                                                    child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                  ),
                                ]
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(Icons.calendar_today_rounded,size: 14,color: Colors.blue,),
                                Text(
                                  ' 6 Requests Expired',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.blue,
                                      fontFamily: "Rubik"
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                  ),
                ),
                Card(
                  color: Color(0xFFedf9fe),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: TextButton(
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => BookmarkEventPage()),);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 20, 10, 5),
                            child: Text("Bookmarked Events",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 20,fontWeight: FontWeight.bold)),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Text("Events which you have bookmarked",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14)),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(Icons.calendar_today_rounded,size: 14,color: Colors.blue,),
                                Text(
                                  ' 10 Events',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.blue,
                                      fontFamily: "Rubik"
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10,0,10,10),
                            child: Row(
                                children:<Widget>[
                                  Expanded(
                                      flex:1,
                                      child: Container(
                                        width: MediaQuery.of(context).size.width * 0.50,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Icon(Icons.person_add_alt_outlined,size: 14,color: Colors.blue,),
                                            Text(
                                              ' 6 Events Expired',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: Colors.blue
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                  ),
                                  Expanded(
                                      flex:1,
                                      child: Container(
                                        width: MediaQuery.of(context).size.width * 0.50,
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              width: MediaQuery.of(context).size.width * 0.20,
                                              child: Stack(
                                                //alignment:new Alignment(x, y)
                                                children: <Widget>[
                                                  new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                                  new Positioned(
                                                    left: 20.0,
                                                    child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                                  ),
                                                  new Positioned(
                                                    left:40.0,
                                                    child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                  ),
                                ]
                            ),
                          ),
                        ],
                      )
                  ),
                ),
              ]
          )
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 60,
          padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.home_outlined,size: 30),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),);
                },
              ),
              IconButton(
                icon: Icon(Icons.calendar_today_rounded,size: 30,color: Colors.blue),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyEventPage()),);
                },
              ),
              IconButton(
                padding: EdgeInsets.all(0),
                alignment: Alignment.topCenter,
                icon: Icon(Icons.add_circle_rounded,size:56),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CreateEvent()),);
                },
              ),
              IconButton(
                icon: Icon(Icons.near_me_sharp,size: 30),
                onPressed: (){},
              ),
              IconButton(
                icon: Icon(Icons.people,size: 30),
                onPressed: (){},
              ),
            ],
          ),
        ),
      ),
      )
    );
  }
}

