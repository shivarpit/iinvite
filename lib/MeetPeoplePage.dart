import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'CreateAccount.dart';
import 'LonelyPage.dart';


final List<String> imagesList = [
  'assets/images/features-1.png',
  'assets/images/features-2.png',
  'assets/images/features-3.png',
];

class MeetPeoplePage extends StatefulWidget {
  @override
  _MeetPeoplePage createState() => _MeetPeoplePage();
}

class _MeetPeoplePage extends State<MeetPeoplePage> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: CarouselSlider(
              options: CarouselOptions(
                  viewportFraction: 0.6,
                  autoPlayAnimationDuration: const Duration(milliseconds: 100),
                  autoPlay: true,
                  enlargeCenterPage: true,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }
              ),
              items: imagesList
                  .map(
                    (item) => Center(
                  child: Image.network(
                    item,
                    fit: BoxFit.cover,
                  ),
                ),
              )
                  .toList(),
            ),
          ),
          Center(
            child: Container(
              height: 50,
            ),
          ),
          Center(
            child: Text('Meet new People!', style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              fontFamily: 'Rubik'
            )
            ),
          ),
          Center(
            child: Container(
              height: 20,
            ),
          ),
          Center(
            child: Text('Lorem ipsum dolor sit amet,', style: TextStyle(
              fontSize: 16,
              color: Colors.grey,
              fontFamily: 'Rubik'
            ),
            ),
          ),
          Center(
            child: Text('consectetur adipiscing elit.', style: TextStyle(
              fontSize: 16,
              color: Colors.grey,
              fontFamily: 'Rubik'
            ),
            ),
          ),
          SizedBox(height: 50),
          Center(
            child: Row(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      height: 50,
                      child: RaisedButton(
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => CreateAccount()),);
                        },
                        child: Text(' Skip ',style: TextStyle(fontFamily: 'Rubik'),),
                        color: Colors.white,
                        textColor: Colors.black,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                      ),
                    )
                ),
                Expanded(
                    child: Container(
                        padding: EdgeInsets.all(10),
                        child: Container(
                            height: 50,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => LonelyPage()),);
                              },
                              child: Text(' Next ',style: TextStyle(fontFamily: 'Rubik'),),
                              color: Colors.lightBlue,
                              textColor: Colors.white,
                              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)
                              ),
                            )
                        )
                    )
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}