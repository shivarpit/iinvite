import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'LoginWebPage.dart';

final List<String> imagesList = [
  'assets/images/features-1.png',
  'assets/images/features-2.png',
  'assets/images/features-3.png',
];

class LonelyPage extends StatefulWidget {
  @override
  _LonelyPage createState() => _LonelyPage();
}

class _LonelyPage extends State<LonelyPage> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: CarouselSlider(
              options: CarouselOptions(
                  viewportFraction: 0.6,
                  autoPlayAnimationDuration: const Duration(milliseconds: 100),
                  autoPlay: true,
                  enlargeCenterPage: true,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }
              ),
              items: imagesList
                  .map(
                    (item) => Center(
                  child: Image.network(
                    item,
                    fit: BoxFit.cover,
                  ),
                ),
              ).toList(),
            ),
          ),
          Center(
            child: Container(
              height: 50,
            ),
          ),
          Center(
            child: Text('No more lonely', style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              fontFamily: 'Rubik'
            )
            ),
          ),
          Center(
            child: Text('Days!', style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              fontFamily: 'Rubik'
            )
            ),
          ),
          Center(
            child: Container(
              height: 20,
            ),
          ),
          Center(
            child: Text('Lorem ipsum dolor sit amet,', style: TextStyle(
              fontSize: 16,
              color: Colors.grey,
              fontFamily: 'Rubik'
            ),
            ),
          ),
          Center(
            child: Text('consectetur adipiscing elit.', style: TextStyle(
              fontSize: 16,
              color: Colors.grey,
              fontFamily: 'Rubik'
            ),
            ),
          ),
          SizedBox(height: 50),
          Center(
            child:Container(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
              child: Container(
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginWebPage()),);
                    },
                    child: Text('Create an account',
                      style: TextStyle(color: Colors.white, fontSize: 18,fontFamily: 'Rubik'),
                    ),
                  )
              )
            )
          )
        ],
      ),
    );
  }
}