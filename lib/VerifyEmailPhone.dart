import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'LoginWebPage.dart';
import 'SecondAccountPage.dart';


class VerifyEmailPhone extends StatefulWidget {
  @override
  _VerifyEmailPhone createState() => _VerifyEmailPhone();
}

class _VerifyEmailPhone extends State<VerifyEmailPhone> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    child: Text("Verify your",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                  ),
                  Container(
                    width: double.infinity,
                    child: Text("account",style: TextStyle(fontFamily: 'Rubik',fontSize: 30,fontWeight: FontWeight.bold),),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    width: double.infinity,
                    child: Text("We send a 6 digit code to your Phone and Email",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      labelText: 'Phone Verification code',
                      hintText: 'Phone Verification code'
                  )
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      labelText: 'Email Verification code',
                      hintText: 'Email Verification code'
                  )
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
              child: Column(
                children: [
                  Container(
                      height: 50,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Done',
                          style: TextStyle(fontFamily: 'Rubik',color: Colors.white, fontSize: 18),
                        ),
                      )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}