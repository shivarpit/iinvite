import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:project/BookmarkEventPage.dart';
import 'package:project/FavoritEventPage.dart';

import 'HomePage.dart';
import 'MyEventPage.dart';

class MyEventListPage extends StatefulWidget {
  const MyEventListPage({Key? key}) : super(key: key);

  @override
  _MyEventListPageState createState() => _MyEventListPageState();
}

class _MyEventListPageState extends State<MyEventListPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.filter_alt_outlined,
                  size: 26.0,
                  color: Colors.black,
                ),
              )
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.notifications_none,
                  size: 26.0,
                  color: Colors.black,
                ),
              )
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Card(
              color: Color(0xFFedf9fe),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
              ),
              child: TextButton(
                onPressed: (){},
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 5),
                      child: Text("My Events",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 20,fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Text("Events which you have created",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14)),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(Icons.calendar_today_rounded,size: 14,color: Colors.blue,),
                          Text(
                            ' 2 Events',
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.blue,
                                fontFamily: "Rubik"
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10,0,10,10),
                      child: Row(
                          children:<Widget>[
                            Expanded(
                                flex:1,
                                child: Container(
                                  width: MediaQuery.of(context).size.width * 0.50,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Icon(Icons.person_add_alt_outlined,size: 14,color: Colors.blue,),
                                      Text(
                                        ' 15 Requests Received',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.blue
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                            ),
                            Expanded(
                                flex:1,
                                child: Container(
                                  width: MediaQuery.of(context).size.width * 0.50,
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(context).size.width * 0.20,
                                        child: Stack(
                                          //alignment:new Alignment(x, y)
                                          children: <Widget>[
                                            new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                            new Positioned(
                                              left: 20.0,
                                              child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                            ),
                                            new Positioned(
                                              left:40.0,
                                              child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                            ),
                          ]
                      ),
                    ),
                  ],
                )
              ),
            ),
            Card(
              color: Color(0xFFedf9fe),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
              ),
              child: TextButton(
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => FavoritEventPage()),);
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 20, 10, 5),
                        child: Text("Favorite Events",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 20,fontWeight: FontWeight.bold)),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Text("Events which you were interested",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14)),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.calendar_today_rounded,size: 14,color: Colors.blue,),
                            Text(
                              ' 2 Events',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.blue,
                                  fontFamily: "Rubik"
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10,0,10,10),
                        child: Row(
                            children:<Widget>[
                              Expanded(
                                  flex:1,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.50,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Icon(Icons.person_add_alt_outlined,size: 14,color: Colors.blue,),
                                        Text(
                                          ' 15 Requests Received',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.blue
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                              ),
                              Expanded(
                                  flex:1,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.50,
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: MediaQuery.of(context).size.width * 0.20,
                                          child: Stack(
                                            //alignment:new Alignment(x, y)
                                            children: <Widget>[
                                              new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                              new Positioned(
                                                left: 20.0,
                                                child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                              ),
                                              new Positioned(
                                                left:40.0,
                                                child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                              ),
                            ]
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.calendar_today_rounded,size: 14,color: Colors.blue,),
                            Text(
                              ' 6 Requests Expired',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.blue,
                                  fontFamily: "Rubik"
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
              ),
            ),
            Card(
              color: Color(0xFFedf9fe),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
              ),
              child: TextButton(
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BookmarkEventPage()),);
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 20, 10, 5),
                        child: Text("Bookmarked Events",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 20,fontWeight: FontWeight.bold)),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Text("Events which you have bookmarked",style: TextStyle(color: Colors.black,fontFamily: "Rubik",fontSize: 14)),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.calendar_today_rounded,size: 14,color: Colors.blue,),
                            Text(
                              ' 10 Events',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.blue,
                                  fontFamily: "Rubik"
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10,0,10,10),
                        child: Row(
                            children:<Widget>[
                              Expanded(
                                  flex:1,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.50,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Icon(Icons.person_add_alt_outlined,size: 14,color: Colors.blue,),
                                        Text(
                                          ' 6 Events Expired',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.blue
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                              ),
                              Expanded(
                                  flex:1,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.50,
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: MediaQuery.of(context).size.width * 0.20,
                                          child: Stack(
                                            //alignment:new Alignment(x, y)
                                            children: <Widget>[
                                              new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                              new Positioned(
                                                left: 20.0,
                                                child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                              ),
                                              new Positioned(
                                                left:40.0,
                                                child: new Icon(Icons.monetization_on, size: 36.0, color: const Color.fromRGBO(218, 165, 32, 1.0)),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                              ),
                            ]
                        ),
                      ),
                    ],
                  )
              ),
            ),
          ]
        )
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 60,
          padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.home_outlined,size: 30),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),);
                },
              ),
              IconButton(
                icon: Icon(Icons.calendar_today_rounded,size: 30,color: Colors.blue),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyEventPage()),);
                },
              ),
              IconButton(
                padding: EdgeInsets.all(0),
                alignment: Alignment.topCenter,
                icon: Icon(Icons.add_circle_rounded,size:56),
                onPressed: (){},
              ),
              IconButton(
                icon: Icon(Icons.near_me_sharp,size: 30),
                onPressed: (){},
              ),
              IconButton(
                icon: Icon(Icons.people,size: 30),
                onPressed: (){},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
