import 'package:flutter/material.dart';
import 'package:project/main.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:delayed_display/delayed_display.dart';

import 'home.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState(){
    super.initState();
    _navigatetohome();
  }

  _navigatetohome()async {
    await Future.delayed(Duration(seconds: 3), () {});
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyHomePage(title: 'iinvite',)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 50,
              width: 300,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: ExactAssetImage('assets/images/logo.png'),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Container(
              height: 15,
            ),
            Container(
              child: DelayedDisplay(
                delay: Duration(seconds: 1),
                child: SpinKitFadingCircle(
                  color: Colors.blue,
                  size: 30.0,
                ),
              ),
            ),
            Container(
              height: 20,
            ),
            Container(
              child: DelayedDisplay(
                delay: Duration(seconds: 1),
                child: Text('Please wait...', style: TextStyle(
                    fontSize: 14
                )
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
