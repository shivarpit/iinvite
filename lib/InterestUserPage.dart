import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:project/IdeaMatchPage.dart';


void main() {
  runApp(InterestUserPage());
}
class InterestUserPage extends StatefulWidget {
  @override
  _InterestUserPage createState() => _InterestUserPage();
}

class _InterestUserPage extends State<InterestUserPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Image.asset(
          "images/logo.png",height: 30,
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Text("Your",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  width: double.infinity,
                  child: Text("Interests",style: TextStyle(fontSize: 30,fontFamily: 'Rubik',fontWeight: FontWeight.bold),),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  width: double.infinity,
                  child: Text("Selected atleast 3 of your interests",style: TextStyle(fontFamily: 'Rubik',fontSize: 18,color: Colors.grey)),
                )
              ],
            ),
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            child: Wrap(
              children: <Widget>[
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () { },
                      child: Text('Movies',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Cooking',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Design',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Gambling',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Music Enthusiast',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Video Games',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Book Nerd',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Art',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Booking',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Athlete',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Shopping',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Technology',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Swimming',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(bottom: 10,right: 10),
                    child: FlatButton(
                      onPressed: () {},
                      child: Text('Videography',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(10)
                      ),
                    )
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: Column(
              children: [
                Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.blue, borderRadius: BorderRadius.circular(10)),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => IdeaMatchPage()),);
                      },
                      child: Text('Continue',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    )
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
